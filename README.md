# JOT

Jörg's Oracle Tool

A command line tool, currently as 'hit and run' (i e the you start the tool
with some parameters, `jot` does its thing and exits).

Some 'REPL' interactive style might come in the future.

As the name suggests this is a tool for my own itches. Maybe it can scratch
yours too.

## Who might want to have a look at this?

Anyone working with Oracle databases. Allthough strictly speaking this tools
does now (can not do) anything that you can't do with some other tool (such as `sqldeveloper`
for example), `jot` might make some things easier.

## Building it

* Windows: MSYS2: `pacman -S gcc pkg-config`
* Install Oracle Instant Client (at least Base and SDK), tested with 12.1/12.2
* update/correct `oci8.pc`
* do this:

     export PKG_CONFIG_PATH=$(pwd)**
* go install gopkg.in/rana/ora.v3
* go build (or install)
* 32bit?
  + export PATH=$PATH:/mingw32/bin
  + env CGO_ENABLED=1 GOOS=windows GOARCH=386 CC="i686-w64-mingw32-gcc -fno-stack-protector -D_FORTIFY_SOURCE=0 -lssp" go build -o jot.exe

## Using it (incomplete!)

Create a jot.toml file. Put it into `~/.config/` or `%USERPROFILE%` on Windows. Adjust to your needs.

## Features

### Implemented
* Ping instances
* Run queries (optional with parameters) from the command line
* Outputs the result of queries as text or Excel-format.
* Does all this on one or several different instances at the same call.

### Planned
* Output as XML
* Extract source files of objects

### Maybe
* Simple scripting language


## Work in progress

* This is not done.
* It is work in progress.
* I implement stuff as I need them myself.
* Some things work.
