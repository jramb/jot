package cmd

/**
2016 by Jörg Ramb
*/

import (
	"fmt"
	"os"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/jramb/jot/tools"
)

var EffectiveTimeNow = time.Now() //.round(time.Minute)

var cfgFile string
var modifyEffectiveTime time.Duration
var RepeatDuration time.Duration
var dBStr string

var verbose bool
var debug bool
var colour bool

func GetEffectiveTime() time.Time {
	effectiveTimeNow := time.Now()

	//if modifyEffectiveTime != nil {
	effectiveTimeNow = effectiveTimeNow.Add(-modifyEffectiveTime).Round(time.Minute)
	//}
	// Rounding is not performed during entry
	//if roundTime != nil {
	//effectiveTimeNow = effectiveTimeNow.Round(RoundTime)
	//}
	tools.D("Effective time: " + effectiveTimeNow.Format("2006-01-02 15:04"))
	return effectiveTimeNow
}

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "jot",
	Short: "Jörg's Oracle Tool!",
	Long:  `Jörg's Oracle Tool!
Collection of several fine tools to be used on an Oracle database.

Made 2016 by Jörg Ramb`,
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	for {
		if err := RootCmd.Execute(); err != nil {
			fmt.Println(err) // printed again here, AFTER the help
			os.Exit(-1)
		}
		if RepeatDuration <= 0 {
			return
		}
		tools.D("Repeat after ", RepeatDuration)
		time.Sleep(RepeatDuration)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports Persistent Flags, which, if defined here,
	// will be global for your application.

	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.config/jot.toml)")
	RootCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "Enables verbose output")
	RootCmd.PersistentFlags().BoolVarP(&debug, "debug", "d", false, "Enables debug output")
	RootCmd.PersistentFlags().BoolVarP(&colour, "colour", "", true, "Enables color output")
	RootCmd.PersistentFlags().StringVarP(&dBStr, "connection", "c", "", "DB connection: <usr>/<pw>@<host>:<port>/<sid>")
	RootCmd.PersistentFlags().DurationVarP(&modifyEffectiveTime, "mod", "m", time.Duration(0), "modify effective time (backwards), eg 7m subtracts 7 minutes")
	RootCmd.PersistentFlags().DurationVarP(&RepeatDuration, "repeat", "r", time.Duration(0), "repeat command, eg 2h repeats every 2 hours")
	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	//RootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	viper.BindPFlag("connection", RootCmd.PersistentFlags().Lookup("connection"))
	viper.BindPFlag("debug", RootCmd.PersistentFlags().Lookup("debug"))
	viper.BindPFlag("verbose", RootCmd.PersistentFlags().Lookup("verbose"))
	viper.BindPFlag("colour", RootCmd.PersistentFlags().Lookup("colour"))

}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" { // enable ability to specify config file via flag
		viper.SetConfigFile(cfgFile)
	}

	viper.SetConfigName("jot")                                      // name of config file (without extension)
	viper.AddConfigPath(".")                                        // current
	viper.AddConfigPath("$HOME/.config")                            // config directory
	viper.AddConfigPath("$HOME")                                    // adding home directory as first search path
	if userprofile := os.Getenv("USERPROFILE"); userprofile != "" { //Windows
		viper.AddConfigPath(userprofile)
	}
	//viper.AutomaticEnv()         // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		tools.D("Using config file: " + viper.ConfigFileUsed())
	} else {
		fmt.Println("Error:", err)
	}
	//viper.Debug()
	//RootCmd.DebugFlags()
}
