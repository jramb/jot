package cmd

/**
2016 by Jörg Ramb
*/

import (
	"errors"
	"fmt"
	"os"
	"path"
	"regexp"
	"strconv"
	"strings"
	"time"

	"gopkg.in/rana/ora.v3"
	// real Excel
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/tealeg/xlsx"
	"gitlab.com/jramb/jot/tools"
)

var outputFile string
var appendFile bool
var extraCol bool
var noHeader bool
var outputType string
var sqlHandle string

func HtmlTD(i interface{}) string {
	switch v := i.(type) {
	case nil:
		return "<td class='N'>&nbsp;</td>"
	case string:
		return "<td class='S'>" + v + "</td>"
	case time.Time:
		//return v.Format("2006-01-02")
		return "<td class='D'>" + v.Format("2006-01-02 15:04:05") + "</td>"
	case ora.OCINum:
		return fmt.Sprintf("<td class='I'>%v</td>", v)
	case int, int64:
		return fmt.Sprintf("<td class='I'>%d</td>", v)
	case float64:
		return fmt.Sprintf("<td class='F'>%f</td>", v)
	default:
		return fmt.Sprintf("<td>%T(%v)</td>", v, v)
	}
}

func ExcelCell(i interface{}) string {
	return HtmlTD(i)
	//switch v := i.(type) {
	//case int, int64:
	//if v.(int64) >= 1e11 {
	//return fmt.Sprintf(`<td class='I'>="%d"</td>`, v)
	//} else {
	//return fmt.Sprintf(`<td class='I'>%d</td>`, v)
	//}
	//default:
	//return HtmlTD(i)
	//}
}

func doReport(cmd *cobra.Command, args []string) error {
	var sqlStr string
	var sqlArgs []string
	if prepSql := viper.GetString("sql"); prepSql != "" {
		// first try to get it from the sqldeveloper file
		sqlStr = sqlDevSQL(prepSql)
		if sqlStr != "" {
			//sqlStr = prepSql // from SQLDeveloper file
			sqlArgs = args
		} else {
			prepSql := viper.GetString("sql." + prepSql)
			if prepSql != "" {
				sqlStr = prepSql // from setup
				sqlArgs = args
			} else {
				if len(args) == 0 {
					return errors.New("Missing or faulty SQL parameter")
				}
				sqlStr = args[0] //strings.Join(args, " ")
				//sqlStr = strings.Join(args, " ")
				sqlArgs = args[1:]
			}
		}
	} else {
		sqlStr = args[0] //strings.Join(args, " ")
		//sqlStr = strings.Join(args, " ")
		sqlArgs = args[1:]
	}
	if extraCol {
		sqlStr = `select (select name from v$database) as "Instance", sysdate, qqq.* from (` + sqlStr + `) qqq`
	}
	tools.D("Executing sql= ", sqlStr, " ", sqlArgs)

	connections := tools.ResolveConnections(viper.GetString("connection"))
	outputType := outputType

	dateRE := regexp.MustCompile(`{(.*)}`)
	if ssm := dateRE.FindStringSubmatch(outputFile); ssm != nil {
		dateFormat := ssm[1]
		if dateFormat == "" {
			dateFormat = "20060102-1504"
		}
		//fmt.Println("Substr found: ", ssm, len(ssm))
		outputFile = dateRE.ReplaceAllString(outputFile, time.Now().Format(dateFormat))
		//fmt.Println("New outputFile: ", outputFile)
	}

	if outputType == "" && outputFile != "" {
		outputType = path.Ext(outputFile)
		if len(outputType) > 0 {
			outputType = outputType[1:] // remove the "."
		}
	}
	tools.D("output type= ", outputType)
	tools.D("output file= ", outputFile)

	if outputType == "xlsx" { // one file, need to open first, loop then
		var file *xlsx.File
		var err error

		file = xlsx.NewFile()
		headerStyle := xlsx.NewStyle()
		headerStyle.Font.Size = 8
		headerStyle.Font.Bold = true
		dataStyle := xlsx.NewStyle()
		dataStyle.Font.Size = 8
		//headerStyle = xlsx.Style{Font: "Arial"}

		for _, singleConnection := range connections {
			tools.D("Connection: ", singleConnection)
			//var sheet *xlsx.Sheet
			sheet, err := file.AddSheet(strings.ToUpper(singleConnection))
			if err != nil {
				return err
			}
			err = tools.WithDBSession(singleConnection, func(ses *ora.Ses, dbName string) error {
				_, err := tools.RunSql(ses, sqlStr, sqlArgs,
					func(h []string) {
						row := sheet.AddRow() // xlsx.Row
						for _, hv := range h {
							cell := row.AddCell() // xlsx.Cell
							cell.Value = hv
							cell.SetStyle(headerStyle)
							//fmt.Println(cell.GetStyle())
						}
					},
					func(i int, r []interface{}) interface{} {
						row := sheet.AddRow() // xlsx.Row
						//for _, v := range tools.ToStringArray(r, tools.DefaultCnv) {
						for _, rv := range r {
							cell := row.AddCell()
							//cell.Value = v
							v := tools.ExcelCnv(rv)
							cell.SetValue(v)
							cell.SetStyle(dataStyle)
						}
						return nil
					})
				return err
			})
			if err != nil {
				return err
			}
		}
		sheet, err := file.AddSheet("SQL")
		row := sheet.AddRow()
		row.AddCell().Value = "Time"
		row.AddCell().Value = time.Now().Format(time.RFC3339)
		row = sheet.AddRow()
		row.AddCell().Value = "SQL"
		row.AddCell().Value = sqlStr
		row = sheet.AddRow()
		for _, v := range sqlArgs {
			row.AddCell().Value = "Params"
			row.AddCell().Value = v
		}
		err = file.Save(outputFile)
		if err != nil {
			return err
		}
	} else { // one file per connection
		for _, singleConnection := range connections {
			tools.D("Connection: ", singleConnection)
			err := tools.WithDBSession(singleConnection, func(ses *ora.Ses, dbName string) error {
				out := os.Stdout
				var err error
				if outputFile != "-" && outputFile != "" {
					if appendFile {
						out, err = os.OpenFile(outputFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
					} else {
						out, err = os.Create(outputFile)
					}
					if err != nil {
						return err
					}
					defer out.Close()
				}
				switch outputType {
				case "tsv":
					data, err := tools.RunSql(ses, sqlStr, sqlArgs, nil,
						func(i int, r []interface{}) interface{} {
							return tools.ToStringArray(r, tools.DefaultCnv)
						})
					if err != nil {
						return err
					}
					for _, r := range data {
						fmt.Fprintln(out, strings.Join(r.([]string), "\t"))
					}
				case "xls":
					fmt.Fprintln(out, "<!--//<![CDATA[")
					fmt.Fprintln(out, sqlStr)
					fmt.Fprintln(out, sqlArgs)
					fmt.Fprintln(out, "//]]>-->")
					fmt.Fprintln(out, "<table>")
					_, err := tools.RunSql(ses, sqlStr, sqlArgs,
						func(h []string) {
							fmt.Fprint(out, `<tr><th>`+
								strings.Join(h, "</th><th>")+
								`</th></tr>`)
						},
						func(i int, r []interface{}) interface{} {
							fmt.Fprint(out, "<tr>"+
								strings.Join(tools.ToStringArray(r, ExcelCell), "")+
								"</tr>")
							return nil
						})
					if err != nil {
						return err
					}
					fmt.Fprintln(out, "</table>")
				case "html":
					fmt.Fprintln(out, "<!--//<![CDATA[")
					fmt.Fprintln(out, sqlStr)
					fmt.Fprintln(out, sqlArgs)
					fmt.Fprintln(out, "//]]>-->")
					fmt.Fprintln(out, `<style>
				th, td { border-bottom: 1px solid #ddd; }
				//tr:nth-child(even) {background-color: #f2f2f2}
				th { text-align: left }
				td.N {background-color: #f2f2f2}
				td.I { text-align: right }
</style>`)
					fmt.Fprintln(out, `<div style="overflow-x:auto;"><table>`)
					_, err := tools.RunSql(ses, sqlStr, sqlArgs,
						func(h []string) {
							fmt.Fprint(out, `<tr><th>`+
								strings.Join(h, "</th><th>")+
								`</th></tr>`)
						},
						func(i int, r []interface{}) interface{} {
							fmt.Fprint(out, "<tr>"+
								strings.Join(tools.ToStringArray(r, HtmlTD), "")+
								"</tr>")
							return nil
						})
					fmt.Fprintln(out, "<table></div>")
					if err != nil {
						return err
					}
				default: // "ascii"
					data, err := tools.RunSql(ses, sqlStr, sqlArgs, nil,
						func(i int, r []interface{}) interface{} {
							return tools.ToStringArray(r, tools.DefaultCnv)
						})
					if err != nil {
						return err
					}
					if noHeader {
						data = data[1:]
					}
					maxW := tools.MaxWidths(data, nil)
					//tools.D(maxW)
					for _, r := range data {
						for i, v := range r.([]string) {
							fmt.Fprintf(out, "%-"+strconv.Itoa(maxW[i])+"v | ", v)
						}
						fmt.Fprintln(out)
					}
				}
				return nil
			})
			if err != nil {
				return nil
			}
		}
	}
	return nil
}

var reportCmd = &cobra.Command{
	Use:   "rep",
	Short: "reports data from a select",
	Long: `Reports/extracts data from one or several databases using SQL statements.
You can choose from different output types using the 'type' flag. If the 'type'
flag is missing but the output file is specified, the type is retrieved using
the extension of the file.

There are two ways to specify the SQL, the first one is by simply passing the
SELECT statement as the first non-flag parameter.
The select can contain parameters (:1, :hej, etc), in that case the values for
the parameters are the following (non-flag) parameters.

The second way is to specify the select in the config file (.toml) and just to refer
to that using the 'sql' flag.

If the file name contains '{...}' then this part is replaced with the current date,
according to the date format description between the brackets. Simple '{}' defaults
to '20060102-1504'.

Example: output-{2006-01-02_15-04-05}.tsv --> output-2016-12-22_09-31-23.tsv `,
	RunE: doReport,
}

func init() {
	RootCmd.AddCommand(reportCmd)

	reportCmd.PersistentFlags().StringVarP(&sqlHandle, "sql", "s", "", "SQL handle (shortcut)")
	reportCmd.PersistentFlags().StringVarP(&outputType, "type", "t", "", "output type: tsv, html, xls, xlsx, ascii")
	reportCmd.PersistentFlags().StringVarP(&outputFile, "out", "o", "-", "output file, '-' means stdout")
	reportCmd.PersistentFlags().BoolVarP(&appendFile, "append", "a", false, "append to the output file (if possible)")
	reportCmd.PersistentFlags().BoolVarP(&extraCol, "extra", "x", false, "add extra convenience column(s) to select")
	reportCmd.PersistentFlags().BoolVarP(&noHeader, "nonames", "n", false, "omit column names (where applicable)")
	viper.BindPFlag("type", reportCmd.PersistentFlags().Lookup("type"))
	viper.BindPFlag("sql", reportCmd.PersistentFlags().Lookup("sql"))

	//headCmd.AddCommand(headAddCmd)
}
