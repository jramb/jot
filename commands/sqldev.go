package cmd

import (
	//"encoding/json"
	"encoding/xml"
	"fmt"
	"os"

	"github.com/spf13/viper"
	//"fmt"
)

// https://www.thepolyglotdeveloper.com/2017/03/parse-xml-data-in-a-golang-application/

type SqlDevRep struct {
	//XMLName     xml.Name        `xml:"displays" json:"-"`
	Name        string          `xml:"name"`
	DisplayList []SqlDevDisplay `xml:"display"`
	FolderList  []SqlDevRep     `xml:"folder"`
}

type SqlDevDisplay struct {
	//XMLName xml.Name `xml:"display" json:"-"`
	Name string `xml:"name"`
	Sql  string `xml:"query>sql"`
}

// TODO: this does not regard folder names
func findDisplay(reps SqlDevRep, reportName string) string {
	for _, d := range reps.DisplayList {
		// fmt.Println(reps.Name, ":", d.Name)
		if d.Name == reportName {
			return d.Sql
		}
		for _, f := range reps.FolderList {
			sql := findDisplay(f, reportName)
			if sql != "" {
				return sql
			}
		}
	}
	return ""
}

func sqlDevSQL(reportName string) string {
	repFile := viper.GetString("sqldevreps")
	if repFile == "" {
		repFile = "~/.sqldeveloper/UserReports.xml"
		// fmt.Println("sqldevreps not set up, must point to the xml file in SQLDeveloper")
		// return ""
	}
	xmlFile, err := os.Open(repFile)
	if err != nil {
		fmt.Println("Error opening file, maybe you need to set up sqldevreps: ", err)
		return ""
	}
	defer xmlFile.Close()

	var sqlDev SqlDevRep

	//xml.Unmarshal(xmlFile, &sqlDev)
	decoder := xml.NewDecoder(xmlFile)
	decoder.Decode(&sqlDev)

	return findDisplay(sqlDev, reportName)
}
