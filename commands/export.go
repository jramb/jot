package cmd

/**
2016 by Jörg Ramb
*/

import (
	"database/sql"
	"fmt"
	"sync"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/jramb/jot/tools"
)

var exportObjects string

func Export(connection string, wg *sync.WaitGroup) {
	defer wg.Done()
	exportLike := viper.GetString("export.like")
	fmt.Printf("Search for %s\n", exportLike)
	_ = tools.WithDB(connection, func(db *sql.DB, dbName string) error {
		err := db.Ping()
		if err != nil {
			fmt.Println("Pinging %s failed: %s\n", dbName, err)
			return err
		}
		rows, err := db.Query(`select name from all_objects where upper(object_name) like upper(:1)`, exportLike)
		if err != nil {
			return err
		}
		defer rows.Close()
		for rows.Next() {
			var name string
			if err := rows.Scan(&name); err != nil {
				return err
			}
			fmt.Printf("Found: %s\n", name)

		}
		return nil
	})
}

var exportCmd = &cobra.Command{
	Use:   "export",
	Short: "export objects from the database",
	Long:  `Exports one or many database objects to the filesystem`,
	RunE: func(cmd *cobra.Command, args []string) error {
		connections := tools.ResolveConnections(viper.GetString("connection"))
		var wg sync.WaitGroup
		//wg.Add(len(connections))
		for _, singleConnection := range connections {
			wg.Add(1)
			go Export(singleConnection, &wg)
		}
		wg.Wait()
		return nil
	},
}

func init() {
	RootCmd.AddCommand(exportCmd)
	exportCmd.PersistentFlags().StringVarP(&exportObjects, "like", "l", "xx", "Objects to export (like style)")
	viper.BindPFlag("export.like", reportCmd.PersistentFlags().Lookup("like"))
}
