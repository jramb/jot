package cmd

/**
2016 by Jörg Ramb
*/

import (
	"database/sql"
	"fmt"
	"sync"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/jramb/jot/tools"
)

func Ping(connection string, wg *sync.WaitGroup) {
	defer wg.Done()
	_ = tools.WithDB(connection, func(db *sql.DB, dbName string) error {
		err := db.Ping()
		now := time.Now().Format("2006-01-02 15:04:05") //time.RFC3339)
		if err != nil {
			fmt.Printf("%s\t%s\t%s\t%s\n", now, dbName, "-", err)
			// fmt.Println("Pinging %s failed: %s\n", dbName, err)
			return err
		}
		var name sql.NullString
		err = db.QueryRow(`select name from v$database`).Scan(&name)
		if name.Valid {
			fmt.Printf("%s\t%s\t%s\tSUCCESS\n", now, dbName, name.String)
		} else {
			fmt.Printf("%s\t%s\t%s\tSUCCESS\n", now, dbName, "?")
			// fmt.Printf("Pinging %s, no name: SUCCESS\n", dbName)
		}
		return nil
	})
}

var pingCmd = &cobra.Command{
	Use:   "ping",
	Short: "ping the database",
	Long:  `Ping (tests) one or several database connections`,
	RunE: func(cmd *cobra.Command, args []string) error {
		connections := tools.ResolveConnections(viper.GetString("connection"))
		var wg sync.WaitGroup
		//wg.Add(len(connections))
		for _, singleConnection := range connections {
			wg.Add(1)
			go Ping(singleConnection, &wg)
		}
		//time.AfterFunc(time.Minute, func() { fmt.Println("Timeout", wg) })
		wg.Wait()
		return nil
	},
}

func init() {
	RootCmd.AddCommand(pingCmd)
}
