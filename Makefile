# SHELL:=bash

default: export

all: run excel html raw

ping:
	go run main.go ping -c all

export:
	go run main.go export -l xxnc_event_pkg

simple:
	go run main.go rep -s test1

sqldevrep1:
	go run main.go rep -c dev3 -x -s 'Testing' --type ascii
#go run main.go rep -c dev3 -x -s 'Num contracts total (day)' --type ascii

xlsx:
	go run main.go rep -c dev3+uat+pdt1 -x -s flex po_headers_all -o test-{}.xlsx
#go run main.go rep -c dev3+uat+prod -s flex po_headers_all -o test.xlsx

ascii:
	go run main.go rep -c dev3+uat -x -s flex po_headers_all -o test-{}.ascii --append

run:
	go run main.go rep -s flex po_headers_all

excel:
	go run main.go rep -s test3 -o test.xls

html:
	go run main.go rep -s test2 -o test.html

raw:
	go run main.go rep 'select :1 from dual' hejsan

build:
	go build

win32:
	env PKG_CONFIG_PATH=$(shell pwd) CGO_ENABLED=1 GOOS=windows GOARCH=386 CC="i686-w64-mingw32-gcc -fno-stack-protector -D_FORTIFY_SOURCE=0 -lssp" go build -o jot32.exe && \
  cp jot32.exe $(GOPATH)/bin/

win:
	env PKG_CONFIG_PATH=$(shell pwd) CGO_ENABLED=1 GOOS=windows GOARCH=amd64 CC="x86_64-w64-mingw32-gcc -fno-stack-protector -D_FORTIFY_SOURCE=0 -lssp" go build -o jot.exe && \
  cp jot.exe $(GOPATH)/bin/

install:
	go install
