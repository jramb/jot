package tools

/**
2016 by Jörg Ramb
*/

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	ora "gopkg.in/rana/ora.v3"

	"github.com/spf13/viper"
	"github.com/ttacon/chalk"
)

func D(args ...interface{}) {
	if viper.GetBool("debug") {
		if viper.GetBool("colour") {
			fmt.Fprintln(os.Stderr, chalk.Cyan.Color(fmt.Sprint(args...)))
		} else {
			fmt.Fprintln(os.Stderr, fmt.Sprint(args...))
		}
	}
}

func ResolveConnections(input string) (list []string) {
	if a := viper.GetString("alias." + input); a != "" {
		input = a
	}
	list = strings.Split(input, "+")
	return
}

var reTrailingZeroes = regexp.MustCompile(`\.0+$`)
var reForbiddenCodes = regexp.MustCompile(`[\t\r\n]`)

func DefaultCnv(i interface{}) string {
	switch v := i.(type) {
	case nil:
		return ""
	case string:
		// return v
		return reForbiddenCodes.ReplaceAllString(v, " ")
	case time.Time:
		t := v.Format("2006-01-02 15:04:05")
		return strings.TrimSuffix(t, " 00:00:00")
	case ora.OCINum:
		return reTrailingZeroes.ReplaceAllString(v.String(), "")
		//return v.String() //fmt.Sprintf("%v", v)
		//return fmt.Sprintf("%v", v)
	case ora.Lob:
		return v.String()
	case int, int64:
		return fmt.Sprintf("%d", v)
	case float64:
		n := fmt.Sprintf("%f", v)
		return reTrailingZeroes.ReplaceAllString(n, "")
	default:
		return fmt.Sprintf("%T(%v)", v, v)
	}
}

func ExcelCnv(i interface{}) interface{} {
	switch v := i.(type) {
	case nil:
		return ""
	case string:
		return v
	case time.Time:
		return v
	case ora.OCINum:
		n := v.String()
		f, err := strconv.ParseFloat(n, 64)
		if err == nil {
			return f
		} else {
			return reTrailingZeroes.ReplaceAllString(n, "")
		}
		// TODO: convert to int64, float64 etc if possible
		//return v.String() //fmt.Sprintf("%v", v)
	case ora.Lob:
		return v.String()
	case int, int64:
		return v
		//return fmt.Sprintf("%d", v)
	case float64:
		return v
		//return fmt.Sprintf("%f", v)
	default:
		return fmt.Sprintf("%T(%v)", v, v)
	}
}

func ToStringArray(iarr []interface{}, cellfn func(i interface{}) string) []string {
	var out []string = make([]string, len(iarr))
	for i, v := range iarr {
		out[i] = cellfn(v)
	}
	return out
}

func Max(a int, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

func MaxWidths(tab []interface{}, minWidths *[]int) []int {
	var currWidths []int
	if minWidths != nil {
		currWidths = *minWidths
	} else {
		currWidths = make([]int, len(tab[0].([]string)))
	}
	for _, r := range tab {
		for i, v := range r.([]string) {
			currWidths[i] = Max(len(v), currWidths[i])
		}
	}
	return currWidths
}
