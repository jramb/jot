package tools

/**
2016 by Jörg Ramb
*/

import (
	"database/sql"
	"fmt"

	"github.com/spf13/viper"
	"gopkg.in/rana/ora.v3"
	// set PKG_CONFIG_PATH=c:\Users\rambj\ownCloud\go\src\gitlab.com\jramb\jot
	// go get
	// see https://godoc.org/gopkg.in/rana/ora.v3
)

func WithDB(connection string, fn func(db *sql.DB, dbName string) error) error {
	//db, err := sql.Open("ora", "xx_fusion_custom/Vis2faskdev3@10.36.254.33:1630/fa3dev.fa3.dev.skanska.se") // "user/passw@host:port/sid")
	//connection := viper.GetString("connection")
	connStr := viper.GetString("connections." + connection)
	if connStr == "" {
		return fmt.Errorf("No such connection defined: %s.", connection)
	}
	db, err := sql.Open("ora", connStr)
	if err != nil {
		return err
	}
	defer db.Close()
	return fn(db, connection)
}

func WithDBSession(connection string, fn func(ses *ora.Ses, dbName string) error) error {
	connStr := viper.GetString("connections." + connection)
	if connStr == "" {
		return fmt.Errorf("No such connection defined: %s.", connection)
	}
	envCfg := ora.NewEnvCfg()
	env, srv, ses, err := ora.NewEnvSrvSes(connStr, envCfg)
	/*
		// This is copied from the source of NewEnvSrvSes
		env, err := ora.OpenEnv()
		if err != nil {
			return err
		}
		var srvCfg ora.SrvCfg
		sesCfg := ora.SesCfg{Mode: ora.DSNMode(connStr)} // dsn
		sesCfg.Username, sesCfg.Password, srvCfg.Dblink = ora.SplitDSN(connStr)
		srv, err := env.OpenSrv(srvCfg)
		if err != nil {
			env.Close()
			return err
		}
		ses, err := srv.OpenSes(sesCfg)
		if err != nil {
			srv.Close()
			env.Close()
			return err
		}
	*/
	if err != nil {
		return err
	}
	defer env.Close()
	defer srv.Close()
	defer ses.Close()

	return fn(ses, connection)
}

/* executes an SQL, fetches all rows, all columns
it processes every row with the given rowProcessor
and appends the result of that to an array for return.
*/
func RunSql(ses *ora.Ses, sqlStr string, args []string,
	headerProcessor func([]string),
	rowProcessor func(int, []interface{}) interface{}) ([]interface{}, error) {
	stmtQry, err := ses.Prep(sqlStr)
	if err != nil {
		return nil, err
	}
	defer stmtQry.Close()
	//rset, err := stmtQry.Qry() //args)
	//a := make([]ora.String, len(args))
	//for i,v := range args {
	//if v=="" {
	//a[i]=ora.String{IsNull: true}
	//} else {
	//a[i]=ora.String{Value: v}
	//}
	//}
	var rset *ora.Rset
	// sad missing feature: need to distinguish these cases myself
	if len(args) > 0 {
		rset, err = stmtQry.Qry(args)
	} else {
		rset, err = stmtQry.Qry()
	}

	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	var ret []interface{} = make([]interface{}, 0, 10)
	rownum := 0
	if headerProcessor != nil {
		headerProcessor(rset.ColumnNames)
	} else {
		ret = append(ret, rset.ColumnNames)
	}
	for rset.Next() {
		rownum++
		procRow := rowProcessor(rownum, rset.Row)
		if procRow != nil {
			ret = append(ret, procRow)
		}
	}
	return ret, nil
}
